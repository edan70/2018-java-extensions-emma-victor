public class NullSafeDotMethodNull {
    public static void main(String[] args) {
        NullSafeDotMethodNull a = null;
        // error: Unexpected error of kind java.lang.NullPointerException
        Object b = a?.m();
        System.out.println(b);
    }

    public String m() {
        return "Simple test works!";
    }
}

/*
 * EXPECT
 *
 */
