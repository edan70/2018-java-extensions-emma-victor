public class NullSafeDotMethodSideEffect {
    public static void main(String[] args) {
        // error: Unexpected error of kind java.lang.NullPointerException
        Increment inc = new Increment();
        System.out.println("i: " + inc?.getI());
    }
}

public class Increment {
    private int i = 0;

    public int getI() {
        i += 1;
        return i;
    }
}

/*
 * EXPECT
 *
 */
