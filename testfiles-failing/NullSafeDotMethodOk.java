public class NullSafeDotMethodOk {
    public static void main(String[] args) {
        NullSafeDotMethodOk a = new NullSafeDotMethodOk();
        // error: Unexpected error of kind java.lang.NullPointerException
        Object b = a?.m();
        System.out.println(b);
    }

    public String m() {
        return "Simple test works!";
    }
}

/*
 * EXPECT
 *
 */
