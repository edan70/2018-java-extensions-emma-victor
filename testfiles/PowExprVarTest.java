public class PowExprVarTest {
    public static void main(String[] args) {
        int x = 4;
        x = (int) (x ** 5);
        System.out.println("4 POW 5: " + x);
    }
}

/*
 * EXPECT
 *
 * 4 POW 5: 1024
 */
