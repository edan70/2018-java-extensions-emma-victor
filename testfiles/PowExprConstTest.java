public class PowExprConstTest {
    public static void main(String[] args) {
        System.out.println("4 POW 5: " + 4 ** 5);
    }
}

/*
 * EXPECT
 *
 * 4 POW 5: 1024.0
 */
