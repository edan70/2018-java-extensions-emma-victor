import java.util.*;

public class ExampleType extends ASTNode {
    private ASTNode children[];

    public ExampleType(String name) {
        super(name);
    }

    public ExampleType(String name, ASTNode children[]) {
        super(name);
        this.children = children;
    }

    public ExampleType treeCopyNoTransform() {
        ExampleType tree = (ExampleType) copy();
        if (children != null) {
            for (int i = 0; i < children.length; ++i) {
                ASTNode child = (ASTNode) children[i];
                if (child != null) {
                    child = child.treeCopyNoTransform();
                    tree.setChild(child, i);
                }
            }
        }
        return tree;
    }

    public ExampleType copy() {
        return new ExampleType(name, children);
    }

    public void setChild(ASTNode node, int i) {
        _children.add(i, node);
    }

    public static void main(String[] args) {
        ASTNode children[] = { new ASTNode("int"), new ASTNode("3") };
        ExampleType a = new ExampleType("assign", children);
        System.out.println(a.treeCopyNoTransform());

        ExampleType b = new ExampleType("12");
        System.out.println(b.treeCopyNoTransform());
    }
}

public class ASTNode {
    protected String name;
    protected List<ASTNode> _children;

    public ASTNode(String name) {
        this.name = name;
        _children = new ArrayList<ASTNode>();
    }

    public ASTNode treeCopyNoTransform() {
        return new ASTNode(name);
    }

    @Override
    public String toString() {
        return name + (_children.size() > 0 ? Arrays.toString(_children.toArray()) : "");
    }
}

/*
 * EXPECT
 *
 * assign[int, 3]
 *
 * 12
 */
