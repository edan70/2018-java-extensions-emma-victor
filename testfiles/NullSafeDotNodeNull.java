public class NullSafeDotNodeNull {
    public static void main(String[] args) {
        Node node = new Node(null, new Node());
        Node child = node?.left?.right;
        String output = child == null ? "null" : child.toString();
        System.out.println(output);
    }
}

public class Node {
    public Node left;
    public Node right;

    public Node() {
    }

    public Node(Node left, Node right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public String toString() {
        if (left != null && right != null) {
            return left.toString() + " : " + right.toString();
        }
        return "-";
    }
}

/*
 * EXPECT
 *
 * null
 */
