public class NullSafeDotPointNull {
    public static void main(String[] args) {
        Point p = null;
        System.out.println("x: " + p?.x + ", y: " + p?.y);
    }
}

public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

/*
 * EXPECT
 *
 * x: null, y: null
 */
