public class NullSafeDotPointOk {
    public static void main(String[] args) {
        Point p = new Point(1, 2);
        System.out.println("x: " + p?.x + ", y: " + p?.y);
    }
}

public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

/*
 * EXPECT
 *
 * x: 1, y: 2
 */
